#!/usr/bin/env bash

docker build \
    -t registry.gitlab.com/jdvr/telegram-personal-bot:latest \
    -t registry.gitlab.com/jdvr/telegram-personal-bot:$(git rev-parse --short HEAD) .