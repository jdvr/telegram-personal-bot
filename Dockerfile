FROM node:12-alpine
COPY . /app
WORKDIR /app
RUN npm install --production && npm run build

FROM node:12-alpine
RUN apk --update add unzip curl bash && \
    curl https://rclone.org/install.sh | bash
COPY --from=0 /app/dist /app/dist
COPY --from=0 /app/node_modules /app/node_modules
ENTRYPOINT NODE_PATH=/app/dist/ node /app/dist/bot.js

