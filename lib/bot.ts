import {TelegramAPI} from 'telegram/api';
import {UpdatesPuller} from 'bot/updates-puller';
import {BotCommands} from 'domain/bot-commands';
import {MessageProcessor} from 'bot/message-processor';
import {YoutubeCommandHandler} from 'commands/youtube';
import {YoutubeDownloader} from 'adapters/youtube';
import {listDirectoryFactory} from './commands/list-directory';
import {RcloneConfig, RcloneFileExplorer} from './adapters/rclone-file-explorer';
import {linkFileFactory} from './commands/link-file';
import {exec} from './adapters/shell-executor';
import {cityWeatherCommandHandlerFactory} from './commands/city-weather';
import {wttrinWeatherApiFactory} from './adapters/wttrin-weather-api';
import {startCommandHandlerFactory} from './commands/start';

const requireEnvVariables = ['TELEGRAM_TOKEN', 'RCLONE_CONFIG_PATH', 'RCLONE_REMOTE', 'PASS_CODE'];

requireEnvVariables.forEach(v => {
    if (process.env[v] == undefined || process.env[v] == '') {
        console.log(`Mandatory environment variable is missing.: \n${requireEnvVariables.join('\n')}`);
        process.exit(1);
    }
});

function processUpdates(update) {
    processor.Process(update);
    return false;
}

const rcloneConfig: RcloneConfig = {
    filePath: process.env.RCLONE_CONFIG_PATH,
    remote: process.env.RCLONE_REMOTE,
};

const telegramApi = new TelegramAPI(process.env.TELEGRAM_TOKEN);
const start = startCommandHandlerFactory(process.env.PASS_CODE);
const youtube = new YoutubeCommandHandler(new YoutubeDownloader());
const listDirectory = listDirectoryFactory(new RcloneFileExplorer(rcloneConfig, exec));
const linkFile = linkFileFactory(new RcloneFileExplorer(rcloneConfig, exec));
const cityWeather = cityWeatherCommandHandlerFactory(wttrinWeatherApiFactory(exec));

const commands = new BotCommands({
    '/start': {handler: start, helpText: 'shows welcome message'},
    '/audio': {handler: youtube.AudioDownload, helpText: 'download audio file'},
    '/list': {handler: listDirectory, helpText: 'list directory'},
    '/link': {handler: linkFile, helpText: 'send download link for a given file full path'},
    '/weather': {handler: cityWeather, helpText: 'send forecast image for a given city name using wttr.in'},
});

const processor = new MessageProcessor(telegramApi, commands);
const puller = new UpdatesPuller(telegramApi, processUpdates);

puller.getUpdates();
