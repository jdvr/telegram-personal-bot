import {
    CommandHandlerInput,
    CreateSendMessageRequest,
    createSendPhotoRequest,
    HandlerResponse,
} from 'domain/command-handlers';
import {WeatherApi} from '../adapters/weather-api';

export const cityWeatherCommandHandlerFactory = (weatherApi: WeatherApi) => async (
    input: CommandHandlerInput
): Promise<HandlerResponse> => {
    const [, city] = input.text.split(' ');

    if (!city) {
        return Promise.resolve(CreateSendMessageRequest('Invalid command, use /help.'));
    }

    if (input.progressListener !== undefined) {
        input.progressListener(CreateSendMessageRequest(`Checking ${city}`));
    }

    try {
        const forecastImage = await weatherApi(city);
        return createSendPhotoRequest(forecastImage.stream, forecastImage.filePath, forecastImage.caption);
    } catch (error) {
        return CreateSendMessageRequest(error.message);
    }
};
