import {CommandHandlerInput, CreateSendMessageRequest, HandlerResponse} from 'domain/command-handlers';
import {BotCommandHandler} from 'domain/bot-commands';
import {FileExplorer} from '../adapters/file-explorer';
import {join} from 'path';

const listDirectoryCommandHandler = (fileExplorer: FileExplorer) => async (
    input: CommandHandlerInput
): Promise<HandlerResponse> => {
    const [, directory] = input.text.split(' ');
    const target = directory || input.repliedMessageText;

    if (!target) {
        return Promise.resolve(CreateSendMessageRequest('Invalid command, use /help.'));
    }

    if (input.progressListener !== undefined) {
        input.progressListener(CreateSendMessageRequest(`Checking ${target}`));
    }

    try {
        const files = await fileExplorer.list(target);

        files
            .map(f => f.path)
            .map(p => join(target, p))
            .forEach(path => input.progressListener(CreateSendMessageRequest(path)));

        return CreateSendMessageRequest('All files listed.');
    } catch (error) {
        return CreateSendMessageRequest(error.message);
    }
};

export const listDirectoryFactory = (fileExplorer: FileExplorer): BotCommandHandler =>
    listDirectoryCommandHandler(fileExplorer);
