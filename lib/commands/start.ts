import {CommandHandlerInput, CreateSendMessageRequest, HandlerResponse} from 'domain/command-handlers';
import {TelegramUser} from '../telegram/entities/responses/get-updates';

const startMessageBody = (from: TelegramUser): string => {
    return `*Hey ${from.first_name}*

Welcome to your personal telegram bot, I have a mix of features, check them using /help`;
};

export const startCommandHandlerFactory = (passCode: string) => async (
    input: CommandHandlerInput
): Promise<HandlerResponse> => {
    const [, code] = input.text.split(' ');

    if (!code || passCode !== code) {
        return Promise.resolve(CreateSendMessageRequest('Invalid start, introduce correct the pass code'));
    }

    return Promise.resolve(CreateSendMessageRequest(startMessageBody(input.from)));
};
