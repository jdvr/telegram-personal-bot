import {FileExplorer, FileInfo} from './file-explorer';
import {ShellExecutor} from './shell-executor';

export interface RcloneConfig {
    filePath: string;
    remote: string;
}

export class RcloneFileExplorer implements FileExplorer {
    private readonly config: RcloneConfig;
    private readonly exec: ShellExecutor;

    public constructor(config: RcloneConfig, exec: ShellExecutor) {
        this.config = config;
        this.exec = exec;
    }

    public async link(filePath: string): Promise<string> {
        try {
            return await this.exec(this.rcloneCommand(`link ${this.config.remote}:${filePath}`));
        } catch (e) {
            console.log(e);
            return Promise.reject({
                message: `Error generating link for ${this.config.remote}:${filePath}`,
            });
        }
    }

    public async list(directory: string = '/'): Promise<FileInfo[]> {
        try {
            const output = await this.exec(this.rcloneCommand(`lsjson ${this.config.remote}:${directory}`));
            return JSON.parse(output).map((p): FileInfo => new FileInfo(Number(p['Size']), p['Path']));
        } catch (e) {
            console.log(e);
            return Promise.reject({
                message: `Error listing ${this.config.remote}:${directory}`,
            });
        }
    }

    private rcloneCommand(command: string): string {
        return `rclone ${command} --config ${this.config.filePath}`;
    }
}
