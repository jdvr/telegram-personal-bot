import {ForecastImage, WeatherApi} from './weather-api';
import axios from 'axios';
import * as fs from 'fs';
import {ShellExecutor} from './shell-executor';

const wttrInPhotoUrl = city => `https://wttr.in/${city}_1.png`;
const wttrInCaptionUrl = city => `https://wttr.in/${city}?format=3`;

async function downloadImage(url: string, path: string) {
    const writer = fs.createWriteStream(path);

    const response = await axios({
        url,
        method: 'GET',
        responseType: 'stream',
    });

    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
        writer.on('finish', resolve);
        writer.on('error', reject);
    });
}

export const wttrinWeatherApiFactory = (exec: ShellExecutor): WeatherApi => async (
    city: string
): Promise<ForecastImage> => {
    const filePath = `/tmp/${city}_${new Date().getTime()}.png`;
    const photoUrl = wttrInPhotoUrl(city);
    const captionUrl = wttrInCaptionUrl(city);
    try {
        await downloadImage(photoUrl, filePath);
        const caption = await exec(`curl ${captionUrl}`);
        return new ForecastImage(fs.createReadStream(filePath), filePath, caption);
    } catch (e) {
        console.error(e);
        throw new Error(`Unnable to fetch time for ${city}`);
    }
};
