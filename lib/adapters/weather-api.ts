import {Readable} from 'stream';

export class ForecastImage {
    public stream: Readable;
    public filePath: string;
    public caption: string;

    public constructor(readableStream: Readable, filePath: string, caption: string) {
        this.stream = readableStream;
        this.filePath = filePath;
        this.caption = caption;
    }
}

export type WeatherApi = (city: string) => Promise<ForecastImage>;
