import * as commandline from 'node-cmd';
import {promisify} from 'util';

export type ShellExecutor = (command: string) => Promise<string>;

export const exec: ShellExecutor = promisify(commandline.get);
